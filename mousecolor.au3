Opt("MouseCoordMode", 1)
Opt("PixelCoordMode", 1)
Opt("TrayIconDebug", 1)
#include <MsgBoxConstants.au3>

hotkeyset("{end}","setit1")
hotkeyset("{del}","setit2")
hotkeyset("{home}","terminate")
global $pos1[2], $color1, $pos2[2], $color2, $sKey

MsgBox($MB_SYSTEMMODAL, "Choose Pixel", "Press HOME at any time to nuke this process." & @LF & "" & @LF & "After closing this message box, mouseover the desired pixel and press END key." & @LF & "" & @LF & "A sound will confirm your selection was valid.")

while 1
If PixelGetColor($pos1[0],$pos1[1]) = $color1 Then
SoundPlay("C:\Windows\media\Speech On.wav")
exitloop
endif
sleep(500)
wend

MsgBox($MB_SYSTEMMODAL, "Choose Color", "Now, choose the color." & @LF & "" & @LF & "After closing this message box, mouseover an area with the desired exact color and press DEL key." & @LF & "" & @LF & "A sound will confirm your selection was valid.")

while 1
If PixelGetColor($pos2[0],$pos2[1]) = $color2 Then
SoundPlay("C:\Windows\media\Speech On.wav")
exitloop
endif
sleep(500)
wend

;clipput("color="&$color&@crlf&"x="&$pos[0]&@crlf&"y="&$pos[1])     ; msgbox that shows values of variables for debugging
$t = MsgBox (4, "Variables Successfully Recorded" ,"Select YES if you want an action to perform when the selected pixel changes FROM the color selected" & @LF & "" & @LF & "Select NO if you want an action to perform when the color of the selected pixel turns TO the chosen color.")
If $t = 6 Then
    $sKey = InputBox("Input Check", "What key do you want pressed when the pixel changes?", "")
	  While 1
	  If PixelGetColor ($pos1[0],$pos1[1]) <> $color2 Then
		 SoundPlay("C:\Windows\media\tada.wav")
		 Send($sKey)											; action when not
		 sleep(100)
		 ExitLoop
	  endif
	  sleep(100)
	  WEnd
ElseIf $t = 7 Then
    $sKey = InputBox("Input Check", "What key do you want pressed when the pixel turns this color?", "")
	  While 1
	  If PixelGetColor ($pos1[0],$pos1[1]) = $color2 Then
		 SoundPlay("C:\Windows\media\tada.wav")
		 Send($sKey)									; action when
		 sleep(100)
		 ExitLoop
	  endif
	  sleep(100)
	  WEnd
EndIf

func setit1()
$pos1=MouseGetPos ( )
$color1=PixelGetColor ( $pos1[0] , $pos1[1])
endfunc

func setit2()
$pos2=MouseGetPos ( )
$color2=PixelGetColor ( $pos2[0] , $pos2[1])
endfunc

func terminate()
   exit 1
endfunc

;	Msgbox (0, "Terminate", "Press HOME to terminate."

;msgbox(1,$color,$pos[0]&@crlf&$pos[1])

; Local $sPasswd InputBox("Security Check", "Enter your password.", "", "*")

;    If PixelGetColor($x1,$y1) = $color1 Then ShellExecute ("C:\Users\Scott\Desktop\Scripts\pixeldetection1\123.txt")

 ;  If PixelGetColor ($pos[0],$pos[1]) = $color Then SoundPlay("C:\Windows\media\tada.wav") and Msgbox (0, "title",  "Hit" & $sKey & "")

; $color1=41704
; $x1=258
; $y1=285

; != is <> in AutoIt

;$x=6
;If Not($x = 5) Then
;  MsgBox(0,"Test","$x isn't equal to 5.")
;Else
;  MsgBox(0,"Test","$x is equal to 5.")
;EndIf



;~ HotKeySet("^{SPACE}", "get_color")
;~ $colorCodeHere =  ;<----------------
;~ func get_color()
;~    Global $point = MouseGetPos()
;~    Global $color = PixelGetColor($point[0], $point[1])
;~    MsgBox(0, "debug", "result: " & $color)
;~ EndFunc


;~ If $intColorGet = $intColor Then
;~     ; Do something here
;~ Else
;~     ; Do something else here
;~ EndIf