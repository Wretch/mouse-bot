Simple program that asks user to select pixel coords and select a color, then hits a user-input hotkey when those conditions are true.

Hotkeys:  
HOME = nuke process  
END = select pixel (on prompt)  
DEL = select color (on prompt)  

Future ideas:  
Choose between a combination of keys, a sequence of keys, launch a file to be added as options.